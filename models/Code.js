const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');
const moment = require('moment');
const uuid = require('uuid/v4')

const codeSchema = new mongoose.Schema({
  code: String,
  oucCode: String,
  slug: String,
  description: String,
  shortDesc: String,
  category: String,
  type: String,
  orderIndex: Number,
  active: {
    type: Boolean,
    default: true
  }
},
{
  timestamps: { 
    createdAt: 'created_at', 
    updatedAt: 'updated_at' 
  }
});

codeSchema.pre('save', function(next) {
  this.slug = slug(uuid(this.code));
  next();
});

module.exports = mongoose.model('Code', codeSchema);