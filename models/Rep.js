const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');
const moment = require('moment');

const repSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    require: 'Error 0013: No Rep Name was provided'
  },
  customer: {
    type: mongoose.Schema.ObjectId,
    ref: 'Customer',
    required: 'Error 0002: No Customer could be linked to the new rep.'
  },
  email: String,
  phone: Number,
  slug: String
},
{
  timestamps: { 
    createdAt: 'created_at', 
    updatedAt: 'updated_at' 
  }
});

repSchema.pre('save', function(next) {
  this.slug = slug(this.name);
  next();
});

function autopopulate(next) {
  this.populate('customer');
  next();
}

repSchema.pre('find', autopopulate);
repSchema.pre('findOne', autopopulate);


module.exports = mongoose.model('Rep', repSchema);