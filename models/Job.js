const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');
const moment = require('moment');
const User = require('./User');

const jobSchema = new mongoose.Schema(
  {
    number: {
      type: Number,
      trim: true,
      required: 'Error 0000: No Job Number was provided.'
    },
    segment: {
      type: Number,
      trim: true,
      default: 0
    },
    name: {
      type: String,
      trim: true,
      required: 'Error 0004: No Job Name was provided.'
    },
    //last modified by and last modified date for this Obj may be needed.
    slug: String,
    customerNumber: {
      type: String,
      trim: true
      // require: 'Error 0001: No Customer Number was provided.'
    },
    path: String,
    customer: {
      type: mongoose.Schema.ObjectId,
      ref: 'Customer',
      required: 'Error 0002: No Customer could be linked to the new job.'
    },
    rep: {
      type: mongoose.Schema.ObjectId,
      ref: 'Rep',
      required: 'Error 0003: No Representative could be linked to the new job'
    },
    project: {
      type: mongoose.Schema.ObjectId,
      ref: 'Project',
      required: 'Error 0003: No Project could be linked to the new job'
    },
    techs: [
      {
        tech: {
          type: mongoose.Schema.ObjectId,
          ref: 'User'
        }
      }
    ],
    type: {
      type: String,
      required: 'Error 0004: No type given to the new job.'
    },
    status: {
      type: String,
      default: 'new'
    },
    location: {
      type: {
        type: String,
        default: 'Point'
      },
      coordinates: [
        {
          type: Number,
          required: 'You must supply coordinates!'
        }
      ],
      googleAddress: {
        type: String,
        required: 'You must supply an address!'
      },
      address: {
        number: {
          type: Number,
          required: 'Error 0005: No Address Number was provided.'
        },
        type: {
          type: String,
          required: 'Error 0006: No Address Number Type was provided.'
        }
      },
      street: {
        name: {
          type: String,
          required: 'Error 0007: No Street Name was provided.'
        },
        type: {
          type: String,
          required: 'Error 0008: No Street Type was provided.'
        }
      },
      city: {
        type: String,
        required: 'Error 0010: No City was provided.'
      },
      county: {
        type: String
      },
      state: {
        type: String,
        default: 'UT',
        required: 'Error 0011: No State was provided.'
      },
      zip: {
        type: Number
        // required: 'Error 0012: No Zip was provided.'
      }
      //last modified by and last modified date for this Obj may be needed.
    },
    notes: String,
    comments: [
      {
        user: {
          type: mongoose.Schema.ObjectId,
          ref: 'User'
        },
        modifiedUser: {
          type: mongoose.Schema.ObjectId,
          ref: 'User'
        },
        comment: String,
        createDate: Date,
        modifiedDate: Date,
        status: {
          type: String,
          default: 'active'
        }
      }
    ],
    dueDate: {
      type: Date,
      default: moment().add(2, 'days')
    },
    startDate: {
      type: Date,
      default: moment().add(1, 'days')
    },
    completeDate: Date,
    parts: [
      {
        part: {
          type: mongoose.Schema.ObjectId,
          ref: 'Part'
        },
        quantity: Number,
        partRecordCretedDate: Date,
        partRecordLastUpdate: Date
        // user?
      }
    ],
    codes: [
      {
        code: {
          type: mongoose.Schema.ObjectId,
          ref: 'Code'
        },
        quantity: Number,
        codeRecordCretedDate: Date,
        codeRecordLastUpdate: Date
        // user?
      }
    ],
    invoice: {
      status: {
        type: String,
        default: 'open'
      },
      invoicedParts: [
        {
          part: {
            type: mongoose.Schema.ObjectId,
            ref: 'Part'
          },
          quantity: Number,
          invoicePartRecordCretedDate: Date,
          invoicePartRecordLastUpdate: Date
        }
      ],
      invoicedCodes: [
        {
          code: {
            type: mongoose.Schema.ObjectId,
            ref: 'Code'
          },
          quantity: Number,
          codeRecordCretedDate: Date,
          codeRecordLastUpdate: Date
        }
      ],
      invoicedDate: Date,
      paidDate: Date,
      sentDate: Date,
      invoiceLastUpdate: Date
    }
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }
);

jobSchema.pre('save', function(next) {
  this.slug = slug(this.number + '-' + this.segment + '-' + this.name);
  next();
});

function autopopulate(next) {
  this.populate('customer');
  this.populate('rep');
  this.populate('project');
  this.populate('Job.parts.part');
  this.populate('parts.part');
  this.populate('Job.comments.user');
  this.populate('Job.comments.modifiedUser');
  this.populate('comments.modifiedUser');
  this.populate('comments.user');
  next();
}

jobSchema.pre('find', autopopulate);
jobSchema.pre('findOne', autopopulate);

module.exports = mongoose.model('Job', jobSchema);
