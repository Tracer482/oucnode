const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');
const moment = require('moment');
const uuid = require('uuid/v4')

const ruleSchema = new mongoose.Schema({
  rule: String, //name of rule
  slug: String, 
  description: String, //what the rule does
  shortDesc: String, //what the rule does short desc
  type: String, //part to part, part to code, code to part, code to code
  index: Number,
  identifyingParts: [
    {
      part: {
        type: mongoose.Schema.ObjectId,
        ref:'Part'
      },
      quantity: Number
    }
  ],
  identifyingCodes: [ 
    {
      code: {
        type: mongoose.Schema.ObjectId,
        ref:'Code'
      },
      quantity: Number
    }
  ],
  expectedParts: [
    {
      part: {
        type: mongoose.Schema.ObjectId,
        ref:'Part'
      },
      quantity: Number
    }
  ],
  expectedCodes: [
    {
      code: {
        type: mongoose.Schema.ObjectId,
        ref:'Code'
      },
      quantity: Number
    }
  ],
  active: {
    type: Boolean,
    default: true
  }
},
{
  timestamps: { 
    createdAt: 'created_at', 
    updatedAt: 'updated_at' 
  }
});

ruleSchema.pre('save', function(next) {
  this.slug = slug(uuid(this.rule));
  next();
});

module.exports = mongoose.model('Rule', ruleSchema);