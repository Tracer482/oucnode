const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');
const moment = require('moment');

const customerSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    require: 'Error 0013: No Customer Name was provided'
  },
  address1: String,
  address2: String,
  city: String,
  state: String,
  zip: Number,
  slug: String
},
{
  timestamps: { 
    createdAt: 'created_at', 
    updatedAt: 'updated_at' 
  }
});

customerSchema.pre('save', function(next) {
  this.slug = slug(this.name);
  next();
});

module.exports = mongoose.model('Customer', customerSchema);