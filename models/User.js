const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;
const md5 = require('md5');
const validator = require('validator');
const mongodbErrorHandler = require('mongoose-mongodb-errors');
const passportLocalMongoose = require('passport-local-mongoose');

const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    validate: [validator.isEmail, 'Invalid Email Address'],
    required: 'Please Supply an email address'
  },
  name: {
    type: String,
    required: 'Please supply a name',
    trim: true
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  project: {
    type: mongoose.Schema.ObjectId,
    ref: 'Project',
    required: 'Error 0002: No Project could be linked to the new rep.'
  },
  customer: {
    type: mongoose.Schema.ObjectId,
    ref: 'Customer',
    required: 'Error 0002: No Customer could be linked to the new rep.'
  },
  Rep: {
    type: mongoose.Schema.ObjectId,
    ref: 'Rep',
    // required: 'Error 0002: No Rep could be linked to the new rep.'
  },
  admin: Boolean,
  tech: Boolean
});

userSchema.virtual('gravatar').get(function() {
  const hash = md5(this.email);
  return `https://gravatar.com/avatar/${hash}?s=200&d=mm&`;
});

userSchema.plugin(passportLocalMongoose, { usernameField: 'email' });
userSchema.plugin(mongodbErrorHandler);

module.exports = mongoose.model('User', userSchema);