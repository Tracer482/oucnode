const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');
const moment = require('moment');

const inboundPartSchema = new mongoose.Schema({
  number: {
    type: Number,
    required: 'Error 0012: No Number Supplied'
  },
  customer: {
    type: mongoose.Schema.ObjectId,
    ref: 'Customer',
    required: 'Error 0014: No Customer could be linked to the new Inbound Part record'
  },
  rep: {
    type: mongoose.Schema.ObjectId,
    ref: 'Rep',
    required: 'Error 0015: No Representative could be linked to the new Inbound Part record'
  },
  parts: [{
    part: {
      type: mongoose.Schema.ObjectId,
      ref:'Part'
    },
    quantity: Number,
    partRecordCretedDate: Date,
    partRecordLastUpdate: Date
  }],
  notes: String,
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User'
  }
},
{
  timestamps: { 
    createdAt: 'created_at', 
    updatedAt: 'updated_at' 
}
})

module.exports = mongoose.model('InboundPart', inboundPartSchema);