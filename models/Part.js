const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const slug = require('slugs');
const moment = require('moment');

const partSchema = new mongoose.Schema({
  cifa: Number,
  oucpid: Number,
  slug: String,
  description: String,
  shortname: String,
  category: String,
  type: String,
  invQty: {
    type: Number,
    default: 0
  },
  invIndex: Number,
  orderIndex: Number,
  active: {
    type: Boolean,
    default: true
  }
},
{
  timestamps: { 
    createdAt: 'created_at', 
    updatedAt: 'updated_at' 
  }
});

partSchema.pre('save', function(next) {
  this.slug = slug(this.cifa+'-'+this.oucpid);
  next();
});

module.exports = mongoose.model('Part', partSchema);