function autocomplete(input, latInput, lngInput, cityInput, countyInput, stateInput, zipInput) {
  if (!input) return; // skip this fn from running if there is not input on the page
  const dropdown = new google.maps.places.Autocomplete(input);

  dropdown.addListener('place_changed', () => {
    const place = dropdown.getPlace();
    console.log(place);
    latInput.value = place.geometry.location.lat();
    lngInput.value = place.geometry.location.lng();

    cityInput.value = place.address_components.find(comp => comp.types.includes('locality')).long_name;
    countyInput.value = place.address_components.find(comp =>
      comp.types.includes('administrative_area_level_2')
    ).long_name;
    stateInput.value = place.address_components.find(comp =>
      comp.types.includes('administrative_area_level_1')
    ).short_name;
    zipInput.value = place.address_components.find(comp => comp.types.includes('postal_code')).long_name;
  });
  // if someone hits enter on the address field, don't submit the form
  input.on('keydown', e => {
    if (e.keyCode === 13) e.preventDefault();
  });
}

export default autocomplete;
