import '../sass/style.scss';

import autocomplete from './modules/autocomplete';

import { $, $$ } from './modules/bling';

autocomplete($('#googleAddress'), $('#lat'), $('#lng'), $('#city'), $('#county'), $('#state'), $('#zip'));

window.onload = function() {
  $('#googleAddress').setAttribute('autocomplete', '__away');
};
