db.Jobs.aggregate(
  // {$match: {slug: req.params.slug}},
  {$unwind: "$parts"},
  {$project: {
    _id: '$_id',
    part: '$parts.part',
    quantity: '$parts.quantity'
  }},
  {$group: {
    _id: {
      job: '$_id',
      part: '$part'
    },
   quantity: {'$sum': '$quantity'}
  }},
  {$group: {
    _id: '$_id.job',
    usedParts: {
      $push: {
        part: '$_id.part',
        quantity: '$quantity'
      }
    }
  }}
)

db.Jobs.aggregate(
  {$unwind: "$parts"},
  {$project: {
    _id: '$_id',
    part: '$parts.part',
    quantity: '$parts.quantity',
    codes: '$codes'
  }},
  {$group: {
    _id: '$_id.job',
    usedParts: {
      $push: {
        part: '$_id.part',
        quantity: '$quantity'
      }
    },
    codes: {'$first': '$codes'}
  }},
  {$unwind: "$codes"},
  {$project: {
    _id: '$_id',
    usedParts: '$usedParts',
    code: '$codes.code',
    codeQty: '$codes.quantity'
  }},
  {$group: {
    _id: {
      job: '$_id',
      code: '$code'
    },
    codeQty: {'$sum': '$codeQty'},
    usedParts: {'$first': '$usedParts'}
  }},
  {$group: {
    _id: '$_id.job',
    codes: {
       $push: {
        code: '$_id.code',
        codeQty: '$codeQty'
      }   
    },
    usedParts: {'$first': '$usedParts'}
  }}
)
