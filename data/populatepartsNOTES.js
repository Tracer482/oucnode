// https://stackoverflow.com/questions/19222520/populate-nested-array-in-mongoose

exports.addPartToJob = async (req, res) => {
  
  const partProm = Part.findOne({slug: '654321-1151231'})
  const custProm = Customer.findOne({slug: 'google'})
  const repProm = Rep.findOne({slug: 'jason'})

  const [part, cust, rep] = await Promise.all([partProm, custProm, repProm])

  now = new Date

  var job = new Job(
    {
      "customer": cust._id,
      "rep": rep._id,
      "number": 10000,
      "name": "Google Fiber Install Herriman V2.0",
      "customerNumber": "G9548135122",
      "type": "FIBER CONST UG",
      "notes": "New Fiber Install ",
      "__v": 0,
      "invoice": {
        "invoicedParts": [],
        "invoicedCodes": [],
        "status": "open"
      },
      "parts": [
        {
          "part": part._id,
          "quantity": 55,
          "partRecordCretedDate": "2017-12-19T06:15:03.145Z",
          "partRecordLastUpdate": "2017-12-19T06:15:03.145Z"
        }
      ],
      "startDate": "2017-12-19T00:00:00.000Z",
      "dueDate": "2017-12-19T00:00:00.000Z",
      "location": {
        "googleAddress": "Herriman, UT, United States",
        "city": "Herriman",
        "zip": 84096,
        "state": "UT",
        "street": {
          "name": "Cow",
          "type": "Drive"
        },
        "address": {
          "number": 5767,
          "type": "West"
        },
        "coordinates": [
          -112.03299379999999,
          40.5141147
        ],
        "type": "Point"
      },
      "status": "created",
      "segment": 1
    }
  )

  const save = await job.save( function(err) {
    if (!err) {
      Job.find({})
        .populate('customer')
        .populate('rep')
        .populate({
          path: 'parts',
          populate: {
            path: 'part'
          }
        })
        .exec(function(err, jobs) {
          console.log(JSON.stringify(jobs))
        })
    }
  })

  res.json(save)

  // const update = await Job.findOneAndUpdate(
  //   {slug: '10000-1-google-fiber-install-draper'}, 
  //   {$push: {
  //     parts: { 
  //       part: partToAdd._id,
  //       quantity: 55,
  //       partRecordCretedDate: now,
  //       partRecordLastUpdate: now
  //     }
  //   }},
  //   {returnNewDocument : true}
  // )

  // console.log(update)

// db.jobs.findOneAndUpdate(
//     {slug: '10000-1-google-fiber-install-draper'}, 
//     {$push: {
//       parts: { 
//         part: ObjectId("5a1fbdfb1c13164e18f9b9b1"),
//         quantity: 10,
//         partRecordCretedDate: "2017-10-20",
//         partRecordLastUpdate: "2017-10-20"
//       }
//     }},
//     {returnNewDocument : true}
//   )
}