/*
  This is a file of data and helper functions that we can expose and use in our templating function
*/

// FS is a built in module to node that let's us read files from the system we're running on
const fs = require('fs');

// moment.js is a handy library for displaying dates. We need this in our templates to display things like "Posted 5 minutes ago"
exports.moment = require('moment');

// Dump is a handy debugging function we can use to sort of "console.log" our data
exports.dump = (obj) => JSON.stringify(obj, null, 2);

// Making a static map is really long - this is a handy helper function to make one
exports.staticMap = ([lng, lat]) => `https://maps.googleapis.com/maps/api/staticmap?center=${lat},${lng}&zoom=14&size=800x150&key=${process.env.MAP_KEY}&markers=${lat},${lng}&scale=2`;

// inserting an SVG
exports.icon = (name) => fs.readFileSync(`./public/images/icons/${name}.svg`);

// Some details about the site
exports.siteName = `OUC - Build Character`;

exports.menu = [
  { slug: '/jobs', title: 'Jobs', icon: 'job'},
  { slug: '/parts', title: 'Parts', icon: 'part', },
  { slug: '/admin', title: 'Admin', icon: 'admin', },
  { slug: '/reports', title: 'Reports', icon: 'report', },
  { slug: '/map', title: 'Map', icon: 'map', },
];

exports.states = [
  'UT', 'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY'
]

exports.types = [
  'COAX SPLICE',
  'COAX CONST UG',
  'COAX CONST OV',
  'FIBER CONST UG',
  'FIBER CONST OV',
  'FIBER SPLICE',
  'GEN CONST',
  'ELECTRICAL'
]

exports.statuses = [
  'created',
  'ready',
  'inprogress',
  'complete',
  'invoicing',
  'invoiced',
  'hold',
  'cancled',
]

exports.statusControls = [
  { status:'created', adminAvaliableStatus: ['ready', 'hold'], techAvaliableStatus: ['ready'], userAvaliableStatus: ['ready']},
  { status:'ready', adminAvaliableStatus: ['inprogress', 'hold'], techAvaliableStatus: ['inprogres'], userAvaliableStatus: []},
  { status:'inprogress', adminAvaliableStatus: ['ready', 'complete', 'hold'], techAvaliableStatus: ['complete'], userAvaliableStatus: []},
  { status:'complete', adminAvaliableStatus: ['ready', 'inprogress'], techAvaliableStatus: [], userAvaliableStatus: []},
  { status:'invoicing', adminAvaliableStatus: ['ready', 'inprogress'], techAvaliableStatus: [], userAvaliableStatus: []},
  { status:'invoiced', adminAvaliableStatus: [], techAvaliableStatus: [], userAvaliableStatus: []},
  { status:'hold', adminAvaliableStatus: ['ready', 'inprogress', 'complete'], techAvaliableStatus: [], userAvaliableStatus: []},
  { status:'cancled', adminAvaliableStatus: [], techAvaliableStatus: [], userAvaliableStatus: []},
]