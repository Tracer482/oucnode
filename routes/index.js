const express = require('express');
const router = express.Router();
const { catchErrors } = require('../handlers/errorHandlers');
const authController = require('../controllers/authController');
const codeController = require('../controllers/codeController');
const ruleController = require('../controllers/ruleController');
const customerController = require('../controllers/customerController');
const fileController = require('../controllers/fileController');
const inboundPartController = require('../controllers/inboundPartController');
const inventoryController = require('../controllers/inventoryController');
const invoiceController = require('../controllers/invoiceController');
const jobController = require('../controllers/jobController');
const partController = require('../controllers/partController');
const projectController = require('../controllers/projectController');
const repController = require('../controllers/repController');
const siteController = require('../controllers/siteController');
const uploadController = require('../controllers/uploadController');
const userController = require('../controllers/userController');

// temp welcome site - replace with
router.get('/', siteController.welcome);
router.get('/admin', siteController.admin);
router.post('/jsonBody', siteController.jsonBody);
// router.post('/formData',
//   uploadController.handleFormData,
//   jobController.searchJobsJSON
// );
//LOGIN
router.get('/login', userController.loginForm);
router.post('/login', authController.login);
router.get('/logout', authController.logout);
router.get('/register', userController.registerForm);
router.post('/register', userController.validateRegister, userController.register, authController.login);
router.get('/account/:id', userController.getAccountById, userController.renderaccount);
router.get('/accounts', userController.getAccounts, userController.renderaccounts);
router.get('/account', userController.getCurrentUser, userController.renderaccount);
//JOBS
router.get('/jobsjson', jobController.getJobsJSON);
router.post('/jobsjson', jobController.getJobsJSON);
router.get('/jobjson/:slug', jobController.getJobJSON);
router.get('/jobarrsjson/:slug', jobController.getJobArraysJSON);
router.get('/newjob', jobController.newJob);
router.post(
  '/updatejob',
  jobController.popReq,
  jobController.validateJob,
  jobController.createJobDir,
  catchErrors(jobController.createJob)
);
router.get('/jobs', catchErrors(jobController.getJobs), jobController.filterJobs, jobController.displayJobs);
router.get(
  '/job/:slug',
  catchErrors(jobController.getJobBySlug),
  catchErrors(fileController.getJobFiles),
  jobController.displayJob,
  catchErrors(jobController.getJobs),
  jobController.displayJobs
);
router.get('/editjob/:slug', jobController.editJob);
router.post('/updatejob/:id', jobController.popReq, catchErrors(jobController.updateJob));
router.post('/updatejob/simple/:id', catchErrors(jobController.updateJob));
router.post('/uploadphoto/:slug', uploadController.upload, uploadController.write, uploadController.resize);
router.post('/uploadMultiple/:slug', uploadController.uploadMultiple);
//JOBS.COMMENTS
router.post(
  '/job/:slug/addComment',
  jobController.addCommentToJobBySlug,
  jobController.getJobBySlug,
  jobController.returnJobComments
  // jobController.returnJobJson
);
router.post(
  '/job/:slug/editComment',
  jobController.editCommentById,
  jobController.getJobBySlug,
  jobController.returnJobComments
  // jobController.returnJobJson
);
router.post(
  '/job/:slug/removeComment',
  jobController.removeCommentById,
  jobController.getJobBySlug,
  jobController.returnJobComments
  // jobController.returnJobJson
);
//CUSTOMERS
router.get('/newcustomer', customerController.newCustomer);
router.post('/updatecustomer', customerController.createCustomer);
router.get('/editcustomer/:slug', catchErrors(customerController.editCustomer));
router.post('/updatecustomer/:id', catchErrors(customerController.updateCustomer));
router.get(
  '/customers',
  catchErrors(repController.getReps),
  catchErrors(customerController.getCustomers),
  customerController.displayCustomers
);
router.get(
  '/customer/:slug',
  catchErrors(customerController.getCustomerBySlug),
  catchErrors(customerController.getCustomers),
  customerController.displayCustomers
);
//PROJECTS
router.get('/newproject', projectController.newProject);
router.post('/updateproject', projectController.createProject);
router.get('/editproject/:slug', catchErrors(projectController.editProject));
router.post('/updateproject/:id', catchErrors(projectController.updateProject));
router.get('/projects', catchErrors(projectController.getProjects), projectController.displayProjects);
router.get(
  '/project/:slug',
  catchErrors(projectController.getProjectBySlug),
  catchErrors(projectController.getProjects),
  projectController.displayProjects
);
//REPS
router.get('/newrep/:id', repController.newRep);
router.post('/updaterep', repController.createRep);
router.get('/editrep/:id', catchErrors(repController.editRep));
router.post('/updaterep/:id', catchErrors(repController.updateRep));
//RULES
router.get('/newrule', partController.getParts, codeController.getCodes, ruleController.newRule);
router.post(
  '/updaterule',
  // partController.getParts,
  // codeController.getCodes,
  ruleController.createRule
);
router.get(
  '/editrule/:slug',
  // ruleController.getRuleBySlug
  partController.getParts,
  codeController.getCodes,
  ruleController.editRule
);
router.post('/updaterule/:id', ruleController.updateRule);
router.get('/rules', catchErrors(ruleController.getRules), ruleController.displayRules);
router.get(
  '/rule/:slug',
  catchErrors(ruleController.getRuleBySlug),
  catchErrors(ruleController.getRules),
  ruleController.displayRules
);
router.get('/rulesJSON', ruleController.getRulesJSON);
//CODES
router.get('/newcode', codeController.newCode);
router.post('/updatecode', codeController.createCode);
router.get('/editcode/:slug', codeController.editCode);
router.post('/updatecode/:id', codeController.updateCode);
router.get('/codes', catchErrors(codeController.getCodes), codeController.displayCodes);
router.get(
  '/code/:slug',
  catchErrors(codeController.getCodeBySlug),
  catchErrors(codeController.getCodes),
  codeController.displayCodes
);
router.get('/addCodes/', codeController.addCodes);
router.get('/addCodes/:slug', codeController.addCodesByJob);
router.post('/updateUsedCodes', codeController.updateUsedCodes);
router.get('/removeUsedCode/:code&:job&:qty', codeController.removeUsedCodeByJobBySlug);
//PARTS
router.get('/newpart', partController.newPart);
router.post('/updatepart', partController.createPart);
router.get('/editpart/:slug', partController.editPart);
router.post('/updatepart/:id', partController.updatePart);
router.get('/parts', catchErrors(partController.getParts), partController.displayParts);
router.get(
  '/part/:slug',
  catchErrors(partController.getPartBySlug),
  catchErrors(partController.getParts),
  partController.displayParts
);
//MANAGE PARTS PER JOB
router.get('/addParts/', inventoryController.addParts);
router.get('/addParts/:slug', inventoryController.addPartsByJob);
router.post('/updateUsedParts', inventoryController.updateUsedParts);
router.get('/updateQty/:part&:job', inventoryController.updatePartQuantityByJobBySlug);
router.get('/removeUsedPart/:part&:job&:qty', inventoryController.removeUsedPartByJobBySlug);
//RECEIVE INBOUND PARTS
router.get('/inboundparts', inboundPartController.getInboundParts, inboundPartController.displayInboundParts);
router.get('/newinboundpart', inboundPartController.newInboundParts);
router.post('/updateInboundPart', inboundPartController.createInboundParts);
router.get(
  '/inboundpart/:id',
  inboundPartController.getInboundPartById,
  inboundPartController.getInboundParts,
  inboundPartController.displayInboundParts
);
// INVOICE
router.get(
  '/startinvoice/:slug',
  jobController.getJobArrays,
  ruleController.getRules,
  invoiceController.initialInvoiceUpdate
);
// //TEST - MULTI FILE INPUT
// router.get('/multifileinput', uploadController.displayMultiFileInputTestForm)
// router.post('/multifileinput',
//   uploadController.uploadMultiple,
//   uploadController.resizeMultiple,
//   uploadController.postMultiFileInputTest
// )
//TEST - SINGLE FILE INPUT
// router.get('/singlefileinput', uploadController.displaySingleFileInputTestForm)
// router.post('/singlefileinput',
//   uploadController.upload,
//   uploadController.resize,
//   uploadController.postSingleFileInputTest
// )
// //TEST - FS FILE EXPLORER
router.get('/file', fileController.fileTest);
module.exports = router;
