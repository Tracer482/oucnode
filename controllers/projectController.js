const mongoose = require('mongoose');
const Project = mongoose.model('Project');
const Rep = mongoose.model('Rep');

exports.newProject = (req, res) => {
  res.render('newProject', {title: 'Create a New Project'});
}

exports.createProject = async (req, res) => {
  const project = await (new Project(req.body)).save();
  res.redirect(`/project/${project.slug}`)
}

exports.editProject = async (req, res) => {
  const project = await Project.findOne({ slug: req.params.slug});
  res.render('editProject', { title: `Edit ${project.name}`, project})
}

exports.updateProject = async (req, res) => {
  const project = await Project.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
    runValidators: true
  }).exec();
  req.flash('sucess', `Successfully update <strong>${project.name}</strong>.`); //add href to the page for the project that was just updated
  res.redirect(`/project/${project.slug}`) //where to go with this? hmmm.... back to the home page for now..
}

//this would return a list of all cusotmers to then manipulate or display.
exports.getProjects = async (req, res, next) => {
  req.projects = await Project.find();
  return next();
}

exports.displayProjects = (req, res) => {
  res.render('projects', { title: 'Projects', projects: req.projects })
}

exports.jsonProjects = (req, res) => {
  res.json(res.projects)
}

//this would show a page where the project details would be displayed
exports.getProjectBySlug = async (req, res, next) => {
  const project = await Project.findOne({ slug: req.params.slug });
  const reps = await Rep.find({project: project._id});
  if (!project) return next();
  res.render('project', { project, reps, title: project.name});
}