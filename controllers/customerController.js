const mongoose = require('mongoose');
const Customer = mongoose.model('Customer');
const Rep = mongoose.model('Rep');

exports.newCustomer = (req, res) => {
  res.render('newCustomer', {title: 'Add a New Customer'});
}

exports.createCustomer = async (req, res) => {
  const customer = await (new Customer(req.body)).save();
  res.redirect(`/customer/${customer.slug}`)
}

exports.editCustomer = async (req, res) => {
  const customer = await Customer.findOne({ slug: req.params.slug});
  res.render('editCustomer', { title: `Edit ${customer.name}`, customer})
}

exports.updateCustomer = async (req, res) => {
  const customer = await Customer.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
    runValidators: true
  }).exec();
  req.flash('sucess', `Successfully update <strong>${customer.name}</strong>.`); //add href to the page for the customer that was just updated
  res.redirect(`/customer/${customer.slug}`) //where to go with this? hmmm.... back to the home page for now..
}

//this would return a list of all cusotmers to then manipulate or display.
exports.getCustomers = async (req, res, next) => {
  req.customers = await Customer.find();
  return next();
}

exports.displayCustomers = (req, res) => {
  res.render('customers', { title: 'Customers', customers: req.customers })
}

exports.jsonCustomers = (req, res) => {
  res.json(res.customers)
}

//this would show a page where the customer details would be displayed
exports.getCustomerBySlug = async (req, res, next) => {
  const customer = await Customer.findOne({ slug: req.params.slug });
  const reps = await Rep.find({customer: customer._id});
  if (!customer) return next();
  res.render('customer', { customer, reps, title: customer.name});
}