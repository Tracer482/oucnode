const mongoose = require('mongoose')
const Job = mongoose.model('Job')
const Customer = mongoose.model('Customer')
const Rep = mongoose.model('Rep')
const Code = mongoose.model('Code')
const moment = require('moment')

exports.newCode = (req, res) => {
  res.render('newCode', {title: 'Create New Code', code: {}}) 
}

exports.createCode = async (req, res) => {
  const code = await (new Code(req.body)).save();
  res.redirect(`/code/${code.slug}`)
}

exports.editCode = async (req, res) => {
  const code = await Code.findOne({slug: req.params.slug})
  res.render('editCode', {title: `Edit ${code.code} - ${code.shortDesc}`, code}) 
}

exports.updateCode = async (req, res) => {
  const code = await Code.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
    runValidators: true
  }).exec();
  req.flash('sucess', `Successfully updated <strong>${code.code}</strong>.`); //add href to the page for the customer that was just updated
  res.redirect(`/code/${code.slug}`) //where to go with this? hmmm.... back to the home page for now..
}

exports.getCodes = async (req, res, next) => {
  req.codes = await Code.find({})
  return next();
}

exports.displayCodes = (req, res) => {
  res.render('codes', {codes: req.codes, title: 'Codes'})
}

exports.getCodeBySlug = async (req, res) => {
  const code = await Code.findOne({slug: req.params.slug}) 
  if (!code) return next();
  res.render('code', {title: `${code.code} - ${code.shortDesc}`, code})
}

exports.addCodes = async (req, res) => {
  //get jobs to know which job to add code to
  const jobProm = Job.find({status: {$in: ['ready', 'inprogress', 'complete', 'invoicing']}})
  //get list of codes to display
  const codeProm = Code.find({})
  //display add code form
  const [jobs, codes] = await Promise.all([jobProm, codeProm])
  //catch no jobs or codes error
  if (!codes || !jobs) {
    //Need to see what is returned for jobs because !jobs isn't working
    req.flash('error', 'Either no Jobs or Codes were found')
    backURL=req.header('Referer') || '/';
    res.redirect(backURL);
  }
  res.render('addCodes', {title: 'Add Codes', jobs, codes})
}

exports.addCodesByJob = async (req, res) => {
  //get jobs to know which job to add code to
  const jobProm = Job.find({status: {$in: ['ready', 'inprogress', 'complete', 'invoicing']}, slug: req.params.slug})
  //get list of codes to display
  const codeProm = Code.find({})
  //display add code form
  const [jobs, codes] = await Promise.all([jobProm, codeProm])
  //catch no jobs or codes error
  if (!codes || !jobs) {
    //Need to see what is returned for jobs because !jobs isn't working
    req.flash('error', 'Either no Jobs or Codes were found')
    backURL=req.header('Referer') || '/';
    res.redirect(backURL);
  }
  res.render('addCodes', {title: 'Add Codes', jobs, codes})
}

exports.updateUsedCodes = async (req, res) => {
  codes = []
  //rearange codes to pop the array right into place
  for (const code in req.body.codes) {
    if (req.body.codes[code] !="") {
      // console.log(`_id: ${code}, qty: ${req.body.codes[code]}`)
      // // let codeId = mongoose.Types.ObjectId(code)
      // // let qty = req.body.codes[code]
      // // let codeRecordCretedDate = moment().toISOString();
      // // console.log(moment())
      // // let codeRecordLastUpdate = moment().toISOString();
      // // console.log(codeRecordLastUpdate)

      codes.push({
        code: mongoose.Types.ObjectId(code),
        quantity: req.body.codes[code],
        invoiceCodeRecordCretedDate: moment().toISOString(),
        invoiceCodeRecordLastUpdate: moment().toISOString()
      })
    }
  }

  console.log(codes)

  job = await Job.update(
    {slug: req.body.job},
    {$push:{codes: {$each:codes}}}
  )

  console.log(job)

  res.redirect(`/job/${req.body.job}`)
}

exports.removeUsedCodeByJobBySlug = async (req, res) => {
  codeId = await Code.findOne({slug: req.params.code})

  codeToRemove = {
    code: codeId._id,
    quantity: (req.params.qty * -1)
  }

  job = await Job.update(
    {slug: req.params.job},
    {$push:{codes: codeToRemove}}
  )

  res.redirect('back')
}