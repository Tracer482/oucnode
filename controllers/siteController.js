const multer = require('multer')
const multerOptions = {
  storage: multer.memoryStorage()
}
const upload = multer(multerOptions).any()

exports.welcome = (req, res) => {
  res.render('welcome', {title: 'Welcome', subtitle:'Over & Under Construction'})
}

exports.admin = (req, res) => {
  res.render('admin', {title: 'Admin Pages'})
}

exports.jsonBody = (req, res) => {
  console.log(req.body)
  res.json(req.body) 
}

exports.formData = (req, res, next) => {
  upload(req, res, function(err) {
    if (err) {
      console.log(err)
      req.send({err:true, msg:"Err Parsing FormData"})
      return
    }
  res.json(req.body) 
  return 
  })
}