const mongoose = require('mongoose')
const User = mongoose.model('User')
const Project = mongoose.model('Project')
const Customer = mongoose.model('Customer')
const promisify = require('es6-promisify')
const h = require('./../helpers')

exports.loginForm = (req, res) => {
  res.render('login', {title: 'Login'})  
}

exports.registerForm = async (req, res) => {
  const customersPromise = Customer.find({})
  const projectPromise = Project.find({})

  const [customers, projects] = await Promise.all([customersPromise, projectPromise])
  //make sure to have xml request for reps
  res.render('register', {title: 'Register User', projects, customers,body:{}})
}

exports.validateRegister = async (req, res, next) => {
  req.sanitizeBody('name');
  req.checkBody('name', 'You must supply a name!').notEmpty();
  req.checkBody('email', 'That Email is not valid!').isEmail();
  req.sanitizeBody('email').normalizeEmail({
    gmail_remove_dots: false,
    remove_extension: false,
    gmail_remove_subaddress: false
  });
  req.checkBody('password', 'Password Cannot be Blank!').notEmpty();
  req.checkBody('password-confirm', 'Confirmed Password cannot be blank!').notEmpty();
  req.checkBody('password-confirm', 'Oops! Your passwords do not match').equals(req.body.password);

  customerPromise = Customer.findOne({slug: req.body.customer})
  projectPromise = Project.findOne({slug: req.body.project})

  const [custObj, projObj] = await Promise.all([customerPromise, projectPromise])

  req.body.customer = custObj._id
  req.body.project = projObj._id

  const errors = req.validationErrors();
  if (errors) {
    req.flash('error', errors.map(err => err.msg));
    res.redirect('/register');
    return; // stop the fn from running
  }
  next(); // there were no errors!
};

exports.register = async (req, res, next) => {
  const user = new User(req.body);
  const register = promisify(User.register, User);
  try {
    await register(user, req.body.password);
  } catch (err) {
    console.log(err)
    req.flash('error', `Failed to Register: ${err.message}`)
    res.redirect('/register');
    return
  }
  next(); // pass to authController.login
};

exports.getAccountById = async(req, res, next) => {
  req.account = await User.findOne({_id: req.params.id}) 
  return next();
}

exports.getCurrentUser = (req, res, next) => {
  req.account = req.user
  return next();
}

exports.getAccounts = async(req, res, next) => {
  req.accounts = await User.find({})
  console.log(req.accounts)
  return next();
} 

exports.renderaccount = (req, res) => {
  res.render('account', {account: req.account})  
}

exports.renderaccounts = (req, res) => {
  res.render('accounts', {accounts: req.accounts})
}