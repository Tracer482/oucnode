const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Job = mongoose.model('Job');
const Customer = mongoose.model('Customer');
const Project = mongoose.model('Project');
const Rep = mongoose.model('Rep');
const Part = mongoose.model('Part');
const Code = mongoose.model('Code');
const chalk = require('chalk');
const slug = require('slugs');
const fs = require('fs');
const moment = require('moment');

exports.newJob = async (req, res) => {
  lastJob = await Job.aggregate([
    {
      $group: {
        _id: null,
        jobNumber: { $max: '$number' }
      }
    }
  ]);
  console.log(lastJob);
  job = {};
  if (lastJob.length <= 0) {
    job.number = 10000;
  } else {
    job = {
      number: lastJob[0].jobNumber + 1
    };
  }
  //pass list of customers
  const customersPromise = Customer.find({});
  const repsPromise = Rep.find({});
  const projectPromise = Project.find({});

  const [customers, reps, projects] = await Promise.all([customersPromise, repsPromise, projectPromise]);
  //make sure to have xml request for reps
<<<<<<< HEAD
<<<<<<< HEAD
  res.render('newJob', {
    title: 'Add a New Job',
    job,
    customers,
    reps,
    projects
  });
};
=======
  res.render('newjob', {title: 'Add a New Job', job, customers, reps, projects})
=======
  res.render('newJob', {title: 'Add a New Job', job, customers, reps, projects})
>>>>>>> fixing file ref caps
}
>>>>>>> fixing render caps vs lower case

exports.popReq = (req, res, next) => {
  console.log(chalk.cyan('popReq'));
  // if (!req.body.location && !req.body.parts) return next();
  req.body.location = {
    googleAddress: req.body.googleAddress,
    coordinates: [req.body.coordinates0, req.body.coordinates1],
    address: {
      number: req.body.addressNumber,
      type: req.body.addressLabel
    },
    street: {
      name: req.body.streetName,
      type: req.body.streetType
    },
    city: req.body.city,
    state: req.body.state,
    zip: req.body.zip
  };
  // req.body.parts = []
  return next();
};

exports.validateJob = async (req, res, next) => {
  console.log(chalk.cyan('validateJob'));
  console.log(req.body);
  req.sanitizeBody('name');
  req.checkBody('name', 'You must add a Job Name!').notEmpty();
  req.checkBody('project', 'You must add a Job Project!').notEmpty();
  req.checkBody('customer', 'You must add a Job Customer!').notEmpty();
  req.checkBody('rep', 'You must add a Job Rep!').notEmpty();
  req.sanitizeBody('customerNumber');

  const errs = req.validationErrors();
  if (errs) {
    const customersPromise = Customer.find({});
    const repsPromise = Rep.find({});
    const projectPromise = Project.find({});

    const [customers, reps, projects] = await Promise.all([customersPromise, repsPromise, projectPromise]);

    req.body.dueDate = new Date(req.body.dueDate);
    req.body.startDate = new Date(req.body.startDate);

    req.flash(
      'error',
      errs.map(err => err.msg)
    );

    res.render('newJob', {
      title: 'Add a New Job',
      job: req.body,
      newJob: true,
      customers,
      reps,
      projects,
      flashes: req.flash()
    });
    return;
  }
  return next();
};

exports.createJobDir = (req, res, next) => {
  req.body.path = process.env.JOBDIR + `/${slug(req.body.number)}-${slug(req.body.segment)}-${slug(req.body.name)}`;
  console.log(req.body.path);
  if (!fs.existsSync(req.body.path)) {
    fs.mkdirSync(req.body.path);
    console.log('creating New Directory');
  }
  return next();
};

exports.getJobsJSON = (req, res) => {
  jobs = Job.find({})
    .populate({
      path: 'parts'
    })
    .exec(function(err, jobs) {
      var options = {
        path: 'parts.part',
        model: 'Part'
      };

      if (err) return res.json(err);

      Job.populate(jobs, options, function(err, popJobs) {
        res.json(popJobs);
      });
    });
};

// function crx(str){
//   return new RegExp(str, 'gim')
// }

// exports.searchJobsJSON = async (req, res) => {
//   let {
//     number,
//     name,
//     project,
//     customer,
//     customerNumber,
//     type,
//     location,
//     status,
//     techs
//   } = req.body
//     number
//     name = crx(name)
//     project = crx(project)
//     customer = crx(customer)
//     customerNumber = crx(customerNumber)
//     type = crx(type)
//     location = crx(location)
//     status = crx(status)
//     techs = crx(techs)

//   console.log(name)
//   req.jobs = await Job.find({number, name})
//   console.log(req.jobs)
//   res.send(req.jobs)
// }

exports.getJobJSON = async (req, res) => {
  job = await Job.findOne({ slug: req.params.slug });
  if (job.parts.length > 0 && job.codes.length > 0) {
    job = await Job.aggregate([
      { $match: { slug: req.params.slug } },
      { $unwind: '$parts' },
      {
        $project: {
          _id: '$_id',
          part: '$parts.part',
          quantity: '$parts.quantity',
          codes: '$codes'
        }
      },
      {
        $group: {
          _id: {
            job: '$_id',
            part: '$part'
          },
          quantity: { $sum: '$quantity' },
          codes: { $first: '$codes' }
        }
      },
      {
        $group: {
          _id: '$_id.job',
          usedParts: {
            $push: {
              part: '$_id.part',
              quantity: '$quantity'
            }
          },
          codes: { $first: '$codes' }
        }
      },
      { $unwind: '$codes' },
      {
        $project: {
          _id: '$_id',
          usedParts: '$usedParts',
          code: '$codes.code',
          quantity: '$codes.quantity'
        }
      },
      {
        $group: {
          _id: {
            job: '$_id',
            code: '$code'
          },
          quantity: { $sum: '$quantity' },
          usedParts: { $first: '$usedParts' }
        }
      },
      {
        $group: {
          _id: '$_id.job',
          codes: {
            $push: {
              code: '$_id.code',
              quantity: '$quantity'
            }
          },
          usedParts: { $first: '$usedParts' }
        }
      }
    ]).exec(function(err, job) {
      Part.populate(job, { path: 'usedParts.part' }, function(err, job) {
        Code.populate(job, { path: 'codes.code' }, function(err, job) {
          Job.populate(job, { path: '_id' }, function(err, job) {
            req.job = job[0]._id;
            req.job.usedParts = job[0].usedParts;
            req.job.codes = job[0].codes;
            res.json(job);
            return;
          });
        });
      });
    });
    // res.json(job)
    // return
  } else if (job.parts.length > 0) {
    job = await Job.aggregate([
      { $match: { slug: req.params.slug } },
      { $unwind: '$parts' },
      {
        $project: {
          _id: '$_id',
          part: '$parts.part',
          quantity: '$parts.quantity'
        }
      },
      {
        $group: {
          _id: {
            job: '$_id',
            part: '$part'
          },
          quantity: { $sum: '$quantity' }
        }
      },
      {
        $group: {
          _id: '$_id.job',
          usedParts: {
            $push: {
              part: '$_id.part',
              quantity: '$quantity'
            }
          }
        }
      }
    ]).exec(function(err, job) {
      Part.populate(job, { path: 'usedParts.part' }, function(err, job) {
        Job.populate(job, { path: '_id' }, function(err, job) {
          req.job = job[0]._id;
          req.job.usedParts = job[0].usedParts;
          res.json(job);
          return;
        });
      });
    });
    // res.json(job)
    // return
  } else if (job.codes.length > 0) {
    job = await Job.aggregate([
      { $match: { slug: req.params.slug } },
      { $unwind: '$codes' },
      {
        $project: {
          _id: '$_id',
          code: '$codes.code',
          quantity: '$codes.quantity'
        }
      },
      {
        $group: {
          _id: {
            job: '$_id',
            code: '$code'
          },
          quantity: { $sum: '$quantity' }
        }
      },
      {
        $group: {
          _id: '$_id.job',
          codes: {
            $push: {
              code: '$_id.code',
              quantity: '$quantity'
            }
          }
        }
      }
    ]).exec(function(err, job) {
      Code.populate(job, { path: 'codes.code' }, function(err, job) {
        Job.populate(job, { path: '_id' }, function(err, job) {
          req.job = job[0]._id;
          req.job.codes = job[0].codes;
          res.json(job);
          return;
        });
      });
    });
    // res.json(job)
    // return
  } else {
    res.json(job);
    return;
  }
};

exports.createJob = async (req, res) => {
  customerPromise = Customer.findOne({ slug: req.body.customer });
  repPromise = Rep.findOne({ slug: req.body.rep });
  projectPromise = Project.findOne({ slug: req.body.project });

  const [custObj, repObj, projObj] = await Promise.all([customerPromise, repPromise, projectPromise]);

  req.body.customer = custObj._id;
  req.body.rep = repObj._id;
  req.body.project = projObj._id;

  const job = new Job(req.body);
  console.log('req.body:');
  console.log(req.body);
  console.log('job');
  console.log(job);
  const save = await job.save();

  // res.json(req.body)
  res.redirect('/jobs');
};

exports.editJob = async (req, res) => {
<<<<<<< HEAD
  const job = await Job.findOne({ slug: req.params.slug });
  console.log(job.dueDate);
  res.render('editjob', { title: `Edit ${job.name}`, job });
};
=======
  const job = await Job.findOne({ slug: req.params.slug});
  console.log(job.dueDate)
  res.render('editJob', { title: `Edit ${job.name}`, job})
}
>>>>>>> fixing file ref caps

exports.updateJob = async (req, res) => {
  // console.log('update job')
  const job = await Job.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
    runValidators: true
  }).exec();
  console.log(job);
  req.flash('sucess', `Successfully update <strong>${job.name}</strong>.`); //add href to the page for the job that was just updated
  res.redirect(`/job/${job.slug}`);
};

exports.getJobs = async (req, res, next) => {
  console.log('getJobs');
  jobs = await Job.find({})
    .populate({
      path: 'parts'
    })
    .exec(function(err, jobs) {
      var options = {
        path: 'parts.part',
        model: 'Part'
      };

      if (err) return res.json(500);

      Job.populate(jobs, options, function(err, popJobs) {
        req.jobs = popJobs;
        return next();
      });
    });
};

exports.filterJobs = (req, res, next) => {
  console.log('filterJobs');
  //takes in req.jobs
  //removes jobs user shouldn't see
  //replaces req.jobs with new job list
  // console.log(req.user)
  // console.log(req.jobs)
  //if admin show all jobs
  if (req.user.admin) {
    return next();
  }
  //if not admin and tech, show jobs tech is assigned to
  if (req.user.tech) {
    req.jobs = req.jobs
      .filter(job => {
        return job.techs.some(tech => {
          return tech.tech.equals(req.user._id);
        });
      })
      .filter(job => {
        job.status.match(/^(ready|inprogress|created|hold)$/);
      });
    return next();
  }
  //if not addmin and not tech return jobs where user is the rep or the job customer/project matches user customer/project
  req.jobs = req.jobs.filter(job => {
    return job.rep._id == req.user._id || job.customer._id == req.user.customer || job.project._id == req.user.project;
  });
  return next();
};

exports.displayJobs = (req, res) => {
  console.log('displayJobs');
  res.render('jobs', { title: 'Jobs', jobs: req.jobs });
};

exports.displayJob = (req, res) => {
  if (req.body.err) return next();
  res.render('job', { job: req.job, title: req.job.name });
};

exports.getJobBySlug = async (req, res, next) => {
  //
  // SEE GET JOB ARRAY FOR HOW TO IMPROVE THIS?
  //
  job = await Job.findOne({ slug: req.params.slug });
  if (job.parts.length > 0 && job.codes.length > 0) {
    job = Job.aggregate([
      { $match: { slug: req.params.slug } },
      { $unwind: '$parts' },
      {
        $project: {
          _id: '$_id',
          part: '$parts.part',
          quantity: '$parts.quantity',
          codes: '$codes'
        }
      },
      {
        $group: {
          _id: {
            job: '$_id',
            part: '$part'
          },
          quantity: { $sum: '$quantity' },
          codes: { $first: '$codes' }
        }
      },
      {
        $group: {
          _id: '$_id.job',
          usedParts: {
            $push: {
              part: '$_id.part',
              quantity: '$quantity'
            }
          },
          codes: { $first: '$codes' }
        }
      },
      { $unwind: '$codes' },
      {
        $project: {
          _id: '$_id',
          usedParts: '$usedParts',
          code: '$codes.code',
          quantity: '$codes.quantity'
        }
      },
      {
        $group: {
          _id: {
            job: '$_id',
            code: '$code'
          },
          quantity: { $sum: '$quantity' },
          usedParts: { $first: '$usedParts' }
        }
      },
      {
        $group: {
          _id: '$_id.job',
          codes: {
            $push: {
              code: '$_id.code',
              quantity: '$quantity'
            }
          },
          usedParts: { $first: '$usedParts' }
        }
      }
    ]).exec(function(err, job) {
      Part.populate(job, { path: 'usedParts.part' }, function(err, job) {
        Code.populate(job, { path: 'codes.code' }, function(err, job) {
          Job.populate(job, { path: '_id' }, function(err, job) {
            req.job = job[0]._id;
            req.job.usedParts = job[0].usedParts;
            req.job.usedCodes = job[0].codes;
            return next();
          });
        });
      });
    });
  } else if (job.parts.length > 0) {
    job = Job.aggregate([
      { $match: { slug: req.params.slug } },
      { $unwind: '$parts' },
      {
        $project: {
          _id: '$_id',
          part: '$parts.part',
          quantity: '$parts.quantity'
        }
      },
      {
        $group: {
          _id: {
            job: '$_id',
            part: '$part'
          },
          quantity: { $sum: '$quantity' }
        }
      },
      {
        $group: {
          _id: '$_id.job',
          usedParts: {
            $push: {
              part: '$_id.part',
              quantity: '$quantity'
            }
          }
        }
      }
    ]).exec(function(err, job) {
      Part.populate(job, { path: 'usedParts.part' }, function(err, job) {
        Job.populate(job, { path: '_id' }, function(err, job) {
          req.job = job[0]._id;
          req.job.usedParts = job[0].usedParts;
          return next();
        });
      });
    });
  } else if (job.codes.length > 0) {
    job = Job.aggregate([
      { $match: { slug: req.params.slug } },
      { $unwind: '$codes' },
      {
        $project: {
          _id: '$_id',
          code: '$codes.code',
          quantity: '$codes.quantity'
        }
      },
      {
        $group: {
          _id: {
            job: '$_id',
            code: '$code'
          },
          quantity: { $sum: '$quantity' }
        }
      },
      {
        $group: {
          _id: '$_id.job',
          codes: {
            $push: {
              code: '$_id.code',
              quantity: '$quantity'
            }
          }
        }
      }
    ]).exec(function(err, job) {
      Code.populate(job, { path: 'codes.code' }, function(err, job) {
        Job.populate(job, { path: '_id' }, function(err, job) {
          req.job = job[0]._id;
          req.job.usedCodes = job[0].codes;
          return next();
        });
      });
    });
  } else {
    req.job = job;
    return next();
  }
};

exports.uploadFile = async (req, res) => {
  const job = await Job.findOne({ slug: req.params.slug });
};

exports.holdJob = async (req, res, next) => {
  const job = await Job.findOneAndUpdate({ slug: req.params.slug }, { status: 'hold' });
  return next();
};

exports.getJobArrays = async (req, res, next) => {
  job = await Job.aggregate([
    { $match: { slug: req.params.slug } },
    {
      $unwind: {
        path: '$parts',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project: {
        _id: '$_id',
        part: '$parts.part',
        quantity: '$parts.quantity',
        codes: '$codes'
      }
    },
    {
      $group: {
        _id: {
          job: '$_id',
          part: '$part'
        },
        quantity: { $sum: '$quantity' },
        codes: { $first: '$codes' }
      }
    },
    {
      $group: {
        _id: '$_id.job',
        usedParts: {
          $push: {
            part: '$_id.part',
            quantity: '$quantity'
          }
        },
        codes: { $first: '$codes' }
      }
    },
    {
      $unwind: {
        path: '$codes',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project: {
        _id: '$_id',
        usedParts: '$usedParts',
        code: '$codes.code',
        quantity: '$codes.quantity'
      }
    },
    {
      $group: {
        _id: {
          job: '$_id',
          code: '$code'
        },
        quantity: { $sum: '$quantity' },
        usedParts: { $first: '$usedParts' }
      }
    },
    {
      $group: {
        _id: '$_id.job',
        codes: {
          $push: {
            code: '$_id.code',
            quantity: '$quantity'
          }
        },
        usedParts: { $first: '$usedParts' }
      }
    }
  ]);
  req.job = job[0];
  return next();
};

exports.getJobArraysJSON = async (req, res) => {
  job = await Job.aggregate([
    { $match: { slug: req.params.slug } },
    {
      $unwind: {
        path: '$parts',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project: {
        _id: '$_id',
        part: '$parts.part',
        quantity: '$parts.quantity',
        codes: '$codes'
      }
    },
    {
      $group: {
        _id: {
          job: '$_id',
          part: '$part'
        },
        quantity: { $sum: '$quantity' },
        codes: { $first: '$codes' }
      }
    },
    {
      $group: {
        _id: '$_id.job',
        usedParts: {
          $push: {
            part: '$_id.part',
            quantity: '$quantity'
          }
        },
        codes: { $first: '$codes' }
      }
    },
    {
      $unwind: {
        path: '$codes',
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $project: {
        _id: '$_id',
        usedParts: '$usedParts',
        code: '$codes.code',
        quantity: '$codes.quantity'
      }
    },
    {
      $group: {
        _id: {
          job: '$_id',
          code: '$code'
        },
        quantity: { $sum: '$quantity' },
        usedParts: { $first: '$usedParts' }
      }
    },
    {
      $group: {
        _id: '$_id.job',
        codes: {
          $push: {
            code: '$_id.code',
            quantity: '$quantity'
          }
        },
        usedParts: { $first: '$usedParts' }
      }
    }
  ]);
  res.json(job[0] || { _id: '', codes: [], usedParts: [] });
};

//Comment controllers

exports.addCommentToJobBySlug = async (req, res, next) => {
  req.body.user = req.user;
  req.body.createDate = Date.now();
  update = await Job.findOneAndUpdate({ slug: req.params.slug }, { $push: { comments: req.body } });
  console.log(update);
  return next();
};

exports.editCommentById = async (req, res, next) => {
  console.log(req.body);
  update = await Job.findOneAndUpdate(
    { slug: req.params.slug, 'comments._id': ObjectId(req.body.id) },
    {
      $set: {
        'comments.$.modifiedUser': req.user,
        'comments.$.modifiedDate': Date.now(),
        'comments.$.comment': req.body.comment
      }
    }
  );
  console.log(update);
  return next();
};

exports.removeCommentById = async (req, res, next) => {
  console.log(req.body);
  update = await Job.findOneAndUpdate(
    { slug: req.params.slug, 'comments._id': ObjectId(req.body.id) },
    {
      $set: {
        'comments.$.status': 'inactive',
        'comments.$.modifiedDate': Date.now(),
        'comments.$.modifiedUser': req.user
      }
    }
  );
  console.log(update);
  return next();
};

exports.returnJobComments = (req, res) => {
  res.send(req.job.comments);
};

exports.renderComment = (req, res) => {
  res.render('comment', { comment: req.comment });
};

exports.renderComments = (req, res) => {
  res.render('comments', { comment: req.comments });
};
