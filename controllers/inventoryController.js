const mongoose = require('mongoose')
const Job = mongoose.model('Job')
const Customer = mongoose.model('Customer')
const Rep = mongoose.model('Rep')
const Part = mongoose.model('Part')
const moment = require('moment')

exports.addParts = async (req, res) => {
  //get jobs to know which job to add part to
  const jobProm = Job.find({status: {$in: ['ready', 'inprogress', 'complete', 'invoicing']}})
  //get list of parts to display
  const partProm = Part.find({})
  //display add part form
  const [jobs, parts] = await Promise.all([jobProm, partProm])
  //catch no jobs or parts error
  if (!parts || !jobs) {
    //Need to see what is returned for jobs because !jobs isn't working
    req.flash('error', 'Either no Jobs or Parts were found')
    backURL=req.header('Referer') || '/';
    res.redirect(backURL);
  }
  res.render('addParts', {title: 'Add Parts', jobs, parts})
}

exports.addPartsByJob = async (req, res) => {
  //get list of parts to display
  //display add part form
  //get jobs to know which job to add part to
  const jobProm = Job.find({status: {$in: ['ready', 'inprogress', 'complete', 'invoicing']}, slug: req.params.slug})
  //get list of parts to display
  const partProm = Part.find({})
  //display add part form
  const [jobs, parts] = await Promise.all([jobProm, partProm])
  //catch no jobs or parts error
  if (!parts || !jobs) {
    //Need to see what is returned for jobs because !jobs isn't working
    req.flash('error', 'Either no Jobs or Parts were found')
    backURL=req.header('Referer') || '/';
    res.redirect(backURL);
  }
  res.render('addParts', {title: 'Add Parts', jobs, parts})
}

exports.updateUsedParts = async (req, res) => {
  parts = []
  //rearange parts to pop the array right into place
  for (const part in req.body.parts) {
    if (req.body.parts[part] !="") {
      // console.log(`_id: ${part}, qty: ${req.body.parts[part]}`)
      // // let partId = mongoose.Types.ObjectId(part)
      // // let qty = req.body.parts[part]
      // // let partRecordCretedDate = moment().toISOString();
      // // console.log(moment())
      // // let partRecordLastUpdate = moment().toISOString();
      // // console.log(partRecordLastUpdate)

      partInvUpdate = await Part.update(
        {_id: mongoose.Types.ObjectId(part)},
        {$inc: {invQty: (req.body.parts[part] * -1)}}
      )

      console.log(partInvUpdate)

      parts.push({
        part: mongoose.Types.ObjectId(part),
        quantity: req.body.parts[part],
        invoicePartRecordCretedDate: moment().toISOString(),
        invoicePartRecordLastUpdate: moment().toISOString()
      })
    }
  }

  console.log(parts)

  job = await Job.update(
    {slug: req.body.job},
    {$push:{parts: {$each:parts}}}
  )

  console.log(job)

  res.redirect(`/job/${req.body.job}`)
}

exports.updatePartQuantityByJobBySlug = (req, res) => {
  res.send(`${req.params.part} ${req.params.job}`)
}

exports.removeUsedPartByJobBySlug = async (req, res) => {
  partId = await Part.findOne({slug: req.params.part})

  partToRemove = {
    part: partId._id,
    quantity: (req.params.qty * -1),
    invoicePartRecordCretedDate: moment().toISOString(),
    invoicePartRecordLastUpdate: moment().toISOString()
  }

  job = await Job.update(
    {slug: req.params.job},
    {$push:{parts: partToRemove}}
  )

  partUpdate = await Part.update(
    {slug: req.params.part},
    {$inc: {invQty: req.params.qty}}
  )

  console.log(partUpdate)

  res.redirect('back')
}