const mongoose = require('mongoose')
const Job = mongoose.model('Job')
const Customer = mongoose.model('Customer')
const Rep = mongoose.model('Rep')
const Part = mongoose.model('Part')
const Code = mongoose.model('Code')
const Rule = mongoose.model('Rule')
const moment = require('moment')

exports.newRule = (req, res) => {
  res.render('newRule', {title: 'Create New Rule', rule: {}, parts: req.parts, codes: req.codes}) 
}

exports.createRule = async (req, res) => {
  req.body.identifyingParts = await promiseAllProps(arrGetPartCodeProm(objToArr(req.body.identifyingParts)))
  req.body.identifyingCodes = await promiseAllProps(arrGetPartCodeProm(objToArr(req.body.identifyingCodes)))
  req.body.expectedParts = await promiseAllProps(arrGetPartCodeProm(objToArr(req.body.expectedParts)))
  req.body.expectedCodes = await promiseAllProps(arrGetPartCodeProm(objToArr(req.body.expectedCodes)))
  rule = await (new Rule(req.body)).save()
  res.redirect('/rules')
}

exports.editRule = async (req, res) => {
  console.log(req.params.slug)
  const rule = await Rule.findOne({slug: req.params.slug})
  res.render('editRule', {title: `Edit Rule `, rule, parts: req.parts, codes: req.codes}) 
}

exports.updateRule = async (req, res) => {
  req.body.identifyingParts = await promiseAllProps(arrGetPartCodeProm(objToArr(req.body.identifyingParts)))
  req.body.identifyingCodes = await promiseAllProps(arrGetPartCodeProm(objToArr(req.body.identifyingCodes)))
  req.body.expectedParts = await promiseAllProps(arrGetPartCodeProm(objToArr(req.body.expectedParts)))
  req.body.expectedCodes = await promiseAllProps(arrGetPartCodeProm(objToArr(req.body.expectedCodes)))
  const rule = await Rule.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
    runValidators: true
  }).exec();
  req.flash('sucess', `Successfully updated Rule.`); //add href to the page for the customer that was just updated
  res.redirect(`/rule/${rule.slug}`) //where to go with this? hmmm.... back to the home page for now..
}

exports.getRules = async (req, res, next) => {
  req.rules = await Rule.find({})
  return next();
}

exports.displayRules = (req, res) => {
  res.render('rules', {rules: req.rules, title: 'Rules'})
}

exports.getRuleBySlug = async (req, res) => {
  const rule = await Rule.findOne({slug: req.params.slug}) 
  if (!rule) return next();
  res.render('rule', {title: `${rule.rule} - ${rule.shortDesc}`, rule})
}

exports.displayRule = (req, res) => {
  res.render('rules', {rules: req.rules, title: 'Rules'})
}

exports.getRulesJSON = async (req, res) => {
  rules = await Rule.find({})
  res.json(rules)
}

function objToArr(obj) {
  console.log('objToArr')
  arr = [];
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) {
      arr.push(obj[key]);
    }
  }
  console.log(arr)
  return arr;
}

function arrGetPartCodeProm(arr) {
  console.log('arrGetPartCodeProm')
  array = arr.map(obj => {
    if (!obj.expected) {return false}
    if (obj.part) {
      obj.part = Part.findOne({slug: obj.part})
    } else if (obj.code) {
      obj.code = Code.findOne({slug: obj.code})
    } else {
      return false
    }
    return obj;
  }).filter(obj => {
    return obj
  })
  console.log(array)
  return array
}

function promiseAllProps(arrayOfObjects) {
  console.log('promiseAllProps')
  let datum = [];
  let promises = [];

  arrayOfObjects.forEach(function(obj, index) {
    Object.keys(obj).forEach(function(prop) {
      let val = obj[prop];
      if (val && val.then) {
        promises.push(val);
        datum.push({obj: obj, prop: prop});
      }
    });
  });
  return Promise.all(promises).then(function(results) {
    results.forEach(function(val, index) {
      let info = datum[index];
      info.obj[info.prop] = val;
    });
    console.log(arrayOfObjects)
    return arrayOfObjects;
  });
}