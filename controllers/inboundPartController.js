const mongoose = require('mongoose');
const Job = mongoose.model('Job');
const Customer = mongoose.model('Customer');
const Project = mongoose.model('Project');
const Rep = mongoose.model('Rep');
const Part = mongoose.model('Part');
const InboundPart = mongoose.model('InboundPart')
const slug = require('slugs');
const moment = require('moment')
const fs = require('fs')

exports.newInboundParts = async (req, res) => {
  const customersPromise = Customer.find({})
  const repsPromise = Rep.find({})
  const partPromise = Part.find({})
  const inboundPartPromise = InboundPart.aggregate([
    {$group: {
      _id: null,
      number: {$max: "$number"}
    }}
  ])

  const [customers, reps, parts, lastInboundPart] = await Promise.all([customersPromise, repsPromise, partPromise, inboundPartPromise])

  console.log(lastInboundPart)
  inboundPart = {}
  if (lastInboundPart.length) {
    if (lastInboundPart[0].number) {
      inboundPart.number = lastInboundPart[0].number+1
    } else {
      inboundPart.number = 1000
    }
  } else {
    inboundPart.number = 1000
  }

  res.render('newInboundParts', {title: 'Add New Inbound Parts', parts, customers, reps, inboundPart})
}

exports.createInboundParts = async (req, res) => {

  customerPromise = Customer.findOne({slug: req.body.customer})
  repPromise = Rep.findOne({slug: req.body.rep})

  const [custObj, repObj] = await Promise.all([customerPromise, repPromise])

  req.body.customer = custObj._id
  req.body.rep = repObj._id

  parts = []
  //rearange parts to pop the array right into place
  for (const part in req.body.parts) {
    if (req.body.parts[part] != '') {
      partInvUpdate = await Part.update(
        {_id: mongoose.Types.ObjectId(part)},
        {$inc: {invQty: (req.body.parts[part])}}
      )
      console.log(partInvUpdate)
      parts.push({
        part: mongoose.Types.ObjectId(part),
        quantity: req.body.parts[part],
        invoicePartRecordCretedDate: moment().toISOString(),
        invoicePartRecordLastUpdate: moment().toISOString()
      })
    }
  }

  req.body.parts = parts
  inboundPart = new InboundPart(req.body)
  const save = await inboundPart.save()

  res.redirect('/inboundParts')
}

exports.getInboundParts = async (req, res, next) => {
  req.inboundParts = await InboundPart.find({})
  return next();
}

exports.displayInboundParts = (req, res) => {
  res.render('inboundParts', {title: 'Inbound Part Records', inboundParts:req.inboundParts})
}

exports.getInboundPartById = async (req, res) => {
  const inboundPart = await InboundPart.findOne({_id: req.params.id}).populate('parts.part')
  console.log(inboundPart)
  if (!inboundPart) return next();
  res.render('inboundPart', {title:`Inbound Part: ${inboundPart.number}`, inboundPart})
}