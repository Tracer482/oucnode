const mongoose = require('mongoose')
const Job = mongoose.model('Job')
const Customer = mongoose.model('Customer')
const Rep = mongoose.model('Rep')
const Part = mongoose.model('Part')
const Code = mongoose.model('Code')

exports.newPart = (req, res) => {
  res.render('newPart', {title: 'Create New Part', part: {}}) 
}

exports.createPart = async (req, res) => {
  const part = await (new Part(req.body)).save();
  res.redirect(`/part/${part.slug}`)
}

exports.editPart = async (req, res) => {
  const part = await Part.findOne({slug: req.params.slug})
  res.render('editPart', {title: `Edit ${part.cifa} - ${part.description}`, part}) 
}

exports.updatePart = async (req, res) => {
  const part = await Part.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
    runValidators: true
  }).exec();
  req.flash('sucess', `Successfully updated <strong>${part.description}</strong>.`); //add href to the page for the customer that was just updated
  res.redirect(`/part/${part.slug}`) //where to go with this? hmmm.... back to the home page for now..
}

exports.getParts = async (req, res, next) => {
  req.parts = await Part.find({})
  return next();
}

exports.displayParts = (req, res) => {
  res.render('parts', {parts: req.parts, title: 'Parts'})
}

exports.getPartBySlug = async (req, res) => {
  const part = await Part.findOne({slug: req.params.slug}) 
  if (!part) return next();
  res.render('part', {title: `${part.cifa} - ${part.description}`, part})
}