const mongoose = require('mongoose');
const moment = require('moment')
const Job = mongoose.model('Job');
const Customer = mongoose.model('Customer');
const Project = mongoose.model('Project');
const Rep = mongoose.model('Rep');
const Part = mongoose.model('Part');
const slug = require('slugs');
const fs = require('fs');

exports.initialInvoiceUpdate = async (req, res, next) => {

//first start - part Phose
  // 1. set status to 'invoicing part phase'
  // 2. consolidate part records to invoice parts
  // -- one record for each part type in invoice parts arr
  // -- -- DOES THE SAME THING NEED TO BE DONE FOR CODES
  // 3. calculate missing parts from rules
  // -- SWITCH IN THEORY CALCLUATE MISSING PARTS AND CODES FROM RULES?
  // 4. display 
  // -- parts/CODES used
  // -- parts/CODES expected based on (for confirmation)

  //update job flow to reflect IN INVOICE state

  invoiceCodes = req.job.codes.map(code => {
    codeArr = {
      code: code.code,
      quantity: code.codeQty,
      codeRecordCretedDate: moment().valueOf(),
      codeRecordLastUpdate: moment().valueOf()
    }
    return codeArr
  })  

  console.log('invoiceCodes')
  console.log(invoiceCodes)

  invoieCodeIds = invoiceCodes.map(code => {
    return code.code
  })

  console.log('invoieCodeIds')
  console.log(invoieCodeIds)

  identifyingCodeRules = req.rules.filter(rule => {
    return rule.identifyingCodes.some(ec => {
      return invoieCodeIds.some(ic => {
        return ic.equals(ec.code)
      })
    })
  })

  console.log('identifyingCodeRules')
  console.log(identifyingCodeRules)

  invoiceParts = req.job.usedParts.map(part => {
    partArr = {
      part: part.part,
      quantity: part.quantity,
      invoicePartRecordCretedDate: moment().valueOf(),
      invoicePartRecordLastUpdate: moment().valueOf()
    }
    return partArr
  })

  console.log('invoiceParts')
  console.log(invoiceParts)

  invoicePartIds = invoiceParts.map(part => {
    return part.part
  })

  console.log('invoicePartIds')
  console.log(invoicePartIds)

  identifyingPartRules = req.rules.filter(rule => {
    return rule.identifyingParts.some(ep => {
      return invoicePartIds.some(ip => {
        return ip.equals(ep.part)
      })
    })
  })  

  console.log('identifyingPartRules')
  console.log(identifyingPartRules)

  identifyingPartRules.forEach(rule => {
    rule.identifyingParts.forEach(part => {
      invoiceParts.forEach(invoicePart => {
        console.log('rule')
        console.log(rule.rule)
        console.log('invoice part == rule part')
        console.log(part.part.equals(invoicePart.part))
        console.log('invoice part qty')
        console.log(invoicePart.quantity)
        console.log('Rule quantity')
        console.log(part.quantity)
        console.log('rule per part')
        console.log(Math.floor(invoicePart.quantity/part.quantity))
      })
    })
  })

  //THIS IS GOOD CODE - comenting out for testing purposes

  // update = await Job.update(
  //   {slug: req.params.slug}, 
  //   {
  //     $set:{
  //       status: 'invoicing', 
  //       invoice: {
  //         status: 'invoicing',
  //         invoiceLastUpdate: moment().valueOf(),
  //         invoicedCodes: invoiceCodes,
  //         invoicedParts: invoiceParts
  //       }
  //     }
  //   }
  // )

  //end good code

  res.json(identifyingPartRules)
  // return next();
}

exports.calculateExpectedParts = (req, res, next) => {
// req.rules Contains rules from rules export.getRules 
}

//Start Invoice Process - part Phase


//submit - part Phose
  // 1. take in req.body.parts & quantities
  // -- update any parts in the single invoice parts record
  // -- create any part records in the single invoice parts record
  // 2. regenerate calculation of missing parts from rules
  // 3. display
  // -- parts used (this will include updates)
  // -- parts sugested (if any)
  // -- confirmation of parts (if no sugestions)

//continue || re-start || re-open of invoice - part phase
  // 1. regenerate calculation of missing parts from rules
  // 2. dispay
  // -- parts used (this will include updates)
  // -- parts sugested (if any)
  // -- confirmation of parts (if no sugestions)

//Confirmation of parts phase
//move strait to first start of invoice code phase

//Continue Invoice - Code Phase
  
//first start - Code Phase
  // 1. set status to 'invoicing code phase'
  // 2. consolidate code records to invoice codes
  // -- one record for each code type in invoice code arr
  // 3. calculate missing codes from rules
  // 4. display 
  // -- codes recorded
  // -- codes expected based on rules (for confirmation)

//submit - Code Phase
  // 1. take in req.body.codes & quantities
  // -- update any codes in the single invoice codes record
  // -- create any codes records in the single invoice codes record
  // 2. regenerate calculation of missing codes from rules
  // 3. display
  // -- codes used (this will include updates)
  // -- codes sugested (if any)
  // -- confirmation of codes (if no sugestions)

//continue || re-start || re-open of invoice - Code Phase
  // 1. regenerate calculation of missing codes from rules
  // 2. display
  // -- codes used (this will include updates)
  // -- codes sugested (if any)
  // -- confirmation of codes (if no sugestions)

//confirmation of code phase
//WHAT NEEDS TO HAPPEN AT THIS MOMENT??
  // 1. set status to invoiced
  // 2. display invoiced page (job page with invoice modifications)
  // -- this will include invoiced options