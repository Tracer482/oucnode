const mongoose = require('mongoose');
const Rep = mongoose.model('Rep');
const Customer = mongoose.model('Customer');

exports.newRep = (req, res) => {
  res.render('newRep', {title: 'Add a New Rep', customer: req.params.id});
}

exports.createRep = async (req, res) => {
  const customer = await Customer.findOne({_id: req.body.customer})
  const custObj = new Customer(customer)
  const rep = new Rep(req.body)
  rep.customer = custObj._id
  rep.save()
  res.redirect(`/customer/${custObj.slug}`)
}

exports.editRep = async (req, res) => {
  const rep = await Rep.findOne({ _id: req.params.id});
  res.render('editRep', { title: `Edit ${rep.name}`, rep})
}

exports.updateRep = async (req, res) => {
  delete req.body.customer
  const rep = await Rep.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true,
    runValidators: true
  }).exec();
  const customer = await Customer.findOne({_id: rep.customer})
  req.flash('sucess', `Successfully update <strong>${rep.name}</strong>.`); //add href to the page for the rep that was just updated
  res.redirect(`/customer/${customer.slug}`) //where to go with this? hmmm.... back to the home page for now..
}

//this would return a list of all customers to then manipulate or display.
exports.getReps = async (req, res, next) => {
  req.reps = await Rep.find();
  return next();
}

exports.displayReps = (req, res) => {
  res.render('reps', { title: 'Reps', reps: req.reps })
}

exports.jsonReps = (req, res) => {
  res.json(res.reps)
}

//this would show a page where the rep details would be displayed
exports.getRepBySlug = async (req, res, next) => {
  const reps = await Rep.findOne({ slug: req.params.slug });
  if (!rep) return next();
  res.render('rep', { rep, title: rep.name});
}