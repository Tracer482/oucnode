const fs = require('fs')
const path = require('path')
const slug = require('slugs')
const mime = require('mime-types')
const moment = require('moment')
const jobDir = process.env.JOBDIR

exports.getJobFiles = async (req, res, next) => {
  req.job.dir = process.env.JOBDIR + `/${slug(req.job.number.toString())}-${slug(req.job.segment.toString())}-${slug(req.job.name)}`
  if (!fs.existsSync(req.job.dir)) {
    fs.mkdirSync(req.job.dir);
  }
  req.job.files = await showDir(req.job.dir)
  return next();
}

exports.makeJobDir = (req, res, next) => {
  const newDir = process.env.JOBDIR + `/${slug(req.job.number.toString())}-${slug(req.job.segment.toString())}-${slug(req.job.name)}`
  if (!fs.existsSync(newDir)) {
    fs.mkdirSync(newDir);
  }
  return next();
}

function showDir(dir) {
  return new Promise(function(resolve, reject){
    fs.readdir(dir, (err, filesarr) => {
        if (err) { reject(err); }
        let files = []
        filesarr.forEach(file => {
          let fileObj = {}
          fileObj.fullPath = dir+'/'+file
          fileObj.path = fileObj.fullPath.substring(8, fileObj.fullPath.length)
          fileObj.mime = mime.lookup(process.env.JOBDIR+'/1000055-1-draper-bench-install'+file)
          fileObj.name = file.split('.')[0].split('__')[0]
          fileObj.file = file
          fileObj.stats = fs.statSync(fileObj.fullPath)
          fileObj.stats.size = bytesToSize(fileObj.stats.size)
          files.push(fileObj)
        })
        resolve(files);
    })
  });
}

function bytesToSize(bytes) {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
  if (bytes === 0) return 'n/a'
  const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)), 10)
  if (i === 0) return `${bytes} ${sizes[i]})`
  return `${(bytes / (1024 ** i)).toFixed(1)} ${sizes[i]}`
}

exports.fileTest = async (req, res) => {
  const filesArr = await fs.readdirSync(process.env.JOBDIR+'/1000055-1-draper-bench-install')
  let files = []
  filesArr.forEach(file => {
    let fileObj = {}
    fileObj.path = file
    fileObj.mime = mime.lookup(process.env.JOBDIR+'/1000055-1-draper-bench-install'+file)
    files.push(fileObj)
  })
  console.log(files)
  const things = await showDir(process.env.JOBDIR+'/1000055-1-draper-bench-install')
  console.log(things)
  res.json(things)
}